#ifndef MSDL_Plot_H__
#define MSDL_Plot_H__


typedef struct _MSDL_Plot
{
    /* Contains the values of all lines */
    int32_t *line_buffer;
    /* Contains the mapped values of all lines */
    int32_t *mapped_line_buffer;

    /*mapped_line_buffer_render_start and end*/
    int32_t *mlbr_start;
    int32_t *mlbr_end;

    /* Number of lines */
    size_t lines;
    /* Length of the lines */
    size_t line_len; 
    
    /* The x resolution in pixel, e.g. if this is 2
     * every second pixel on the x axis cointains one value */
    size_t resolution;

    /* The maximum number of y values per line, that fit in line_buffer */
    size_t max_line_len;
    /* Maximum of y values that fit in the plot window */
    size_t max_line_len_plot;

    /* Defines the size of the the plot window */
    int32_t maxX, minX;
    int32_t maxY, minY;
    
    /* Defines a offset form minX ... that is used to draw lines */
    int32_t maxX_line_off, minX_line_off;
    int32_t maxY_line_off, minY_line_off;

    /* Max and min y values for mapping */
    int32_t max_value, min_value;

}MSDL_Plot;

MSDL_Plot *MSDL_CreatePlot(int32_t maxX, int32_t minX, int32_t maxY, int32_t minY, size_t lines, size_t max_line_len);
int32_t MSDL_LoadLinesCSV(MSDL_Plot *plot, char* file_name, size_t lines, size_t line_len);
int32_t MSDL_AppendToLines(MSDL_Plot *plot, int32_t *buffer, size_t lines);
void MSDL_DrawPlot(MSDL_Plot *plot, SDL_Renderer* renderer);
void MSDL_IncLineStart(MSDL_Plot* plot);
void MSDL_DecLineStart(MSDL_Plot* plot);
void MSDL_DestroyPlot(MSDL_Plot *plot);
void MSDL_IncLineRes(MSDL_Plot* plot);
void MSDL_DecLineRes(MSDL_Plot* plot);
void MSDL_IncLineMax(MSDL_Plot* plot);
void MSDL_DecLineMax(MSDL_Plot* plot);
void MSDL_ChangeWindowRes(MSDL_Plot * plot, uint32_t w, uint32_t h);


#endif /* MSDL_Plot_H__ */
