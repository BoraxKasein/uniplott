#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <SDL2/SDL.h>

#include "MSDL_Color.h"
#include "MSDL_Plot.h"

#define BORDER_H 80
#define BORDER_W 20

int main(int argc, char** argv){

    int sdl_error = 0;
    SDL_Window *window;
    SDL_Renderer* renderer;
    SDL_Event event;
    SDL_bool done = SDL_FALSE;
    SDL_DisplayMode display;

    size_t lines = 0;
    size_t line_len = 0;

    if (argc != 4)
    {
        printf("Pleas specify file_path, lines and line_len. If line_len is 0 file_path should be a tty:\r\n");
        printf("%s <file_path> <lines> <line_len>\r\n", argv[0]);
        return 1;
    }

    lines = strtol(argv[2], NULL, 10);
    line_len = strtol(argv[3], NULL, 10);


    sdl_error = SDL_CreateWindowAndRenderer(640,
                                            480,
                                            SDL_WINDOW_RESIZABLE | SDL_WINDOW_FULLSCREEN_DESKTOP,
                                            &window,
                                            &renderer);
    if (sdl_error != 0)
    {
        printf("SDL_CreateWindow Error: %s\r\n", SDL_GetError());
        return 1;
    }
    
    SDL_GetCurrentDisplayMode(SDL_GetWindowDisplayIndex(window), &display);
  
    MSDL_Plot *plot = MSDL_CreatePlot(display.w - BORDER_W*2, BORDER_W, BORDER_H, display.h - BORDER_H*2, lines, line_len);
 
    if (MSDL_LoadLinesCSV(plot, argv[1], lines, line_len) != 0)
    {
        printf("Load_CSV Error\r\n");
        return 1;
    }

   
    while (!done)
    {
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderClear(renderer);

        MSDL_DrawPlot(plot, renderer);
        SDL_RenderPresent(renderer);

        while (SDL_PollEvent(&event)) 
        {
            if (event.type == SDL_QUIT) 
            {
                done = SDL_TRUE;
            }
            if (event.key.keysym.sym == SDLK_l)
            {
                MSDL_IncLineStart(plot);
            }
            if (event.key.keysym.sym == SDLK_h)
            {
                MSDL_DecLineStart(plot);
            }
            if (event.key.keysym.sym == SDLK_MINUS ||
                event.key.keysym.sym == SDLK_KP_MINUS)
            {
                if (event.key.repeat == 0 && event.key.state == SDL_RELEASED)
                {
                    MSDL_DecLineRes(plot);
                }
            }
            if (event.key.keysym.sym == SDLK_PLUS ||
                event.key.keysym.sym == SDLK_KP_PLUS)
            {
                if (event.key.repeat == 0 && event.key.state == SDL_RELEASED)
                {
                    MSDL_IncLineRes(plot);
                }
            }
            if (event.key.keysym.sym == SDLK_k)
            {
                //if (event.key.repeat == 0 && event.key.state == SDL_RELEASED)
                {
                    MSDL_IncLineMax(plot);
                }
            }
            if (event.key.keysym.sym == SDLK_j)
            {
                //if (event.key.repeat == 0 && event.key.state == SDL_RELEASED)
                {
                    MSDL_DecLineMax(plot);
                }
            }

            if (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
            {
                uint32_t w = event.window.data1 - BORDER_W*2;
                uint32_t h = event.window.data2 - BORDER_H*2;
                MSDL_ChangeWindowRes(plot, w, h);
            }
        }
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

	SDL_Quit();
	return 0;
}
