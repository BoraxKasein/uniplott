#  Makefile for kalman filtering code

NAME   = uniplott
OBJS   = uniplott.o MSDL_Plot.o MSDL_Color.o
CC 	   = cc

SDL_CFLAGS = -I/usr/include/SDL2 -D_REENTRANT 
SDL_LIBS   = -pthread -lSDL2

CFLAGS = -Wall -g ${SDL_CFLAGS} 
LIBS   = -lm ${SDL_LIBS}

${NAME}: $(OBJS)
	$(CC) ${CFLAGS} $(OBJS) ${LIBS} -o ${NAME}

all: ${NAME}

%.o: %.c
	$(CC) -c $< $(CFLAGS) 

clean:
	rm -f *.o *~ ${NAME} 


