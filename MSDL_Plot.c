#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <SDL2/SDL.h>

#include "MSDL_Color.h"
#include "MSDL_Plot.h"

static long msdl_map(long x, long in_min, long in_max, long out_min, long out_max);
static void msdl_remap_line_buffer(MSDL_Plot *plot);
static void msdl_set_render_draw_color(SDL_Renderer *renderer, MSDL_Color_Name color_name);


MSDL_Plot *MSDL_CreatePlot(int32_t maxX, int32_t minX, int32_t maxY, int32_t minY, size_t lines, size_t max_line_len)
{
    MSDL_Plot *plot = malloc(sizeof(MSDL_Plot));
    if (NULL == plot)
    {
        return NULL;
    }

    plot->line_buffer = malloc(lines * max_line_len * sizeof(int32_t));
    if (NULL == plot->line_buffer)
    {
        MSDL_DestroyPlot(plot);
        return NULL;
    }

    plot->mapped_line_buffer = malloc(lines * max_line_len * sizeof(int32_t));
    if (NULL == plot->line_buffer)
    {
        MSDL_DestroyPlot(plot);
        return NULL;
    }

    plot->mlbr_start = plot->mapped_line_buffer;

    plot->lines = lines;
    plot->line_len = 0U;

    plot->resolution = 20U; 

    plot->max_line_len = max_line_len;
    plot->max_line_len_plot = (maxX - minX) / plot->resolution;

    plot->maxX = maxX;
    plot->minX = minX;
    plot->maxY = maxY;
    plot->minY = minY;

    plot->maxX_line_off = 0;
    plot->minX_line_off = 0;
    plot->maxY_line_off = (plot->minY/20);
    plot->minY_line_off = -(plot->minY/20);

    plot->max_value = INT32_MIN;
    plot->min_value = INT32_MAX;
    
    return plot;
}

int32_t MSDL_LoadLinesCSV(MSDL_Plot *plot, char* file_name, size_t lines, size_t line_len)
{   
    int32_t value;

    if (lines != plot->lines)
        return -1;

    FILE *csv = fopen(file_name, "r");
    if (csv == NULL)
    {
        return -1;
    }

    for (uint32_t ll = 0; ll < line_len; ll++)
    {
        for (uint32_t l = 0; l < lines; l++)
        {
            if (fscanf(csv, "%d,", &value) < 1)
            {
                return -1;
            }
            
            plot->line_buffer[l * line_len + ll] = value;
            if (plot->max_value < value)
                plot->max_value = value;
            if (plot->min_value > value)
                plot->min_value = value;
        }
    }

    plot->line_len = line_len;
    
    msdl_remap_line_buffer(plot);

    return 0;
}

int32_t MSDL_AppendToLines(MSDL_Plot *plot, int32_t *buffer, size_t lines)
{
    for (uint32_t l = 0; l < lines; l++)
    {
        plot->line_buffer[l * plot->line_len] = buffer[l];
        if (plot->max_value < buffer[l])
        {
            plot->max_value = buffer[l];
            msdl_remap_line_buffer(plot);
        }
        if (plot->min_value > buffer[l])
        {
            plot->min_value = buffer[l];
            msdl_remap_line_buffer(plot);
        }
    }

    if (lines > 0)
        plot->line_len++;   
    
    return 0;
}

void MSDL_DrawPlot(MSDL_Plot *plot, SDL_Renderer* renderer)
{
    SDL_Rect clipr = {plot->minX, plot->maxY, plot->maxX, plot->minY-20};
    size_t plot_len = (plot->max_line_len_plot < plot->line_len) ? plot->max_line_len_plot + 2 : plot->line_len;


    msdl_set_render_draw_color(renderer, MSDL_Color_White);
    SDL_RenderDrawRect(renderer, &clipr);
    SDL_RenderSetClipRect(renderer, &clipr);

    for(int l = 0; l < plot->lines; l++)
    {
        msdl_set_render_draw_color(renderer, l + MSDL_Color_White);
        
        for(int ll = 0; ll < plot_len - 1; ll++)
        {
            SDL_RenderDrawLine(renderer, 
                               plot->minX + ll * plot->resolution, 
                               plot->mlbr_start[l * plot->line_len + ll], 
                               plot->minX + ll * plot->resolution + plot->resolution, 
                               plot->mlbr_start[l * plot->line_len + ll + 1]);
        }
    }
}

void MSDL_IncLineStart(MSDL_Plot* plot)
{
    if (plot->mlbr_start + plot->max_line_len_plot + 2 < plot->mapped_line_buffer + plot->line_len)
    {
        plot->mlbr_start++;
    }
}

void MSDL_DecLineStart(MSDL_Plot* plot)
{
    if (plot->mlbr_start > plot->mapped_line_buffer)
    {
        plot->mlbr_start--;
    }
}

void MSDL_IncLineRes(MSDL_Plot* plot)
{
    if (plot->resolution < SIZE_MAX)
    {
        plot->resolution++;
        plot->max_line_len_plot = (plot->maxX - plot->minX) / plot->resolution;
    }
}

void MSDL_DecLineRes(MSDL_Plot* plot)
{
    if (plot->resolution  > 1U)
    {
        plot->resolution--;
        plot->max_line_len_plot = (plot->maxX - plot->minX) / plot->resolution;
    }
}

void MSDL_IncLineMax(MSDL_Plot* plot)
{
//    plot->minY++;
//    plot->maxY--;

    plot->minY_line_off++;
    plot->maxY_line_off--;

    msdl_remap_line_buffer(plot);
   
}

void MSDL_DecLineMax(MSDL_Plot* plot)
{
//    plot->minY--;
//    plot->maxY++;

    plot->minY_line_off--;
    plot->maxY_line_off++;

    msdl_remap_line_buffer(plot);
}

void MSDL_DestroyPlot(MSDL_Plot *plot)
{
    if(NULL != plot)
    {
        if (NULL != plot->line_buffer)
            free(plot->line_buffer);
        if (NULL != plot->mapped_line_buffer)
            free(plot->mapped_line_buffer);

        free(plot);
    }
}

void MSDL_ChangeWindowRes(MSDL_Plot * plot, uint32_t w, uint32_t h)
{
    plot->maxX = w;
    plot->minY = h;

    plot->max_line_len_plot = (plot->maxX - plot->minX) / plot->resolution;

    msdl_remap_line_buffer(plot);
}

static long msdl_map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

static void msdl_remap_line_buffer(MSDL_Plot *plot)
{
    uint32_t line_buffer_size = plot->lines * plot->line_len;

    for (uint32_t l = 0; l < line_buffer_size; l++)
    {
         plot->mapped_line_buffer[l] =  msdl_map(plot->line_buffer[l],
                                                 plot->min_value, 
                                                 plot->max_value,
                                                 plot->minY + plot->minY_line_off, 
                                                 plot->maxY + plot->maxY_line_off);
    }
}

static void msdl_set_render_draw_color(SDL_Renderer *renderer, MSDL_Color_Name color_name)
{
    MSDL_Color color;

    MSDL_SetColor(&color, color_name);
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
}

