#ifndef MSDL_Color_H__
#define MSDL_Color_H__


typedef struct _MSDL_Color
{
    uint8_t r, g, b, a;
}MSDL_Color;

typedef enum 
{
    MSDL_Color_Black = 1,
    MSDL_Color_White,
    MSDL_Color_Red,
    MSDL_Color_Blue,
    MSDL_Color_Yellow,
    MSDL_Color_Orange,
    MSDL_Color_Brown
}MSDL_Color_Name;

void MSDL_SetColor(MSDL_Color *color, MSDL_Color_Name color_name);

#endif /*MSDL_Color_H__*/
