#include <stdint.h>
#include <SDL2/SDL.h>
#include "MSDL_Color.h"

void MSDL_SetColor(MSDL_Color *color, MSDL_Color_Name color_name)
{
    switch(color_name)
    {
        case MSDL_Color_White:
        {
            color->r = 255;
            color->g = 255;
            color->b = 255;
            break;
        }
        case MSDL_Color_Black:
        {
            color->r = 0;
            color->g = 0;
            color->b = 0;
            break;
        }
        case MSDL_Color_Red:
        {
            color->r = 255;
            color->g = 0;
            color->b = 0;
            break;
        }
        case MSDL_Color_Blue:
        {
            color->r = 0;
            color->g = 0;
            color->b = 255;
            break;
        }
        case MSDL_Color_Yellow:
        {
            color->r = 255;
            color->g = 255;
            color->b = 0;
            break;
        }
        case MSDL_Color_Orange:
        {
            color->r = 255;
            color->g = 128;
            color->b = 0;
            break;
        }
        case MSDL_Color_Brown:
        {
            color->r = 102;
            color->g = 51;
            color->b = 0;
            break;
        }
    }
    color->a = SDL_ALPHA_OPAQUE;
}
